const Discord = require("discord.js");
const Client = new Discord.Client({
    intents: ["GUILDS", "GUILD_MESSAGES","DIRECT_MESSAGES"]
});

const prefix = "d!";

Client.on("ready", () => {
    console.log("bot opérationnel");
});

Client.on("messageCreate", message => {
    if(message.author.bot) return;

    //d!help
    if(message.content === prefix+"help"){
        const embed = new Discord.MessageEmbed()
            .setColor("#0099ff")
            .setTitle("Liste de commande")
            .setURL("https://discord.js.org/")
            .setAuthor("Auteur du bot", "https://i.imgur.com/AfFp7pu.png", "https://discord.js.org/")
            .setDescription("Vous y trouverez la liste des commandes")
            .setThumbnail("https://i.imgur.com/AfFp7pu.png") // affiche une images
            .addField("!help", "Affiche la liste des commande")
            .addField("!ping", "Renvoie 'pong !'")
            .setTimestamp()
            .setFooter("Ce bot est créé grace à discord.js", "https://i.imgur.com/AfFp7pu.png");

        message.channel.send({ embeds : [embed]});
    }
    //d!ping
    else if(message.content === prefix+"ping"){
        message.reply("pong !");
    }
});



Client.login("OTQ1Nzc3NDUwMDk5NTQ4MjAy.YhVF4w.Qfls3tfewHuBef8VPu0WdhugWHU");